from __future__ import print_function
import operator

def sortfasta(myfile="scaffold.fa"):
 n = 10

 with open(myfile) as fid:
  d = {}
  seq = 0
  sseq = ""
  entry = fid.readline().rstrip()[1:]
  line = fid.readline().rstrip()
  
  while line:
   if line[0] == ">":
    d[entry] = [seq,sseq]
    seq = 0
    sseq = ""
    entry = line[1:]
   else:
    seq += len(line)
    sseq += line
   line = fid.readline().rstrip()
  else:
   d[entry] = [seq,sseq]
 sorted_d = sorted(d.items(), key=operator.itemgetter(1),reverse=True)
 a = ""
 if n > len(d):
  n = len(d)
 mysize = 0
 for i in range(0,n):
  if mysize >= 20000:
   break
  a += "{1}\n".format(sorted_d[i][0],sorted_d[i][1][1])
  mysize += sorted_d[i][1][0]
 return a

if __name__ == "__main__":
 import sys
 a = sortfasta(sys.argv[1])

